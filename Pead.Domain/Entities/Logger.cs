﻿using Pead.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pead.Domain.Entities
{
    public class Logger
    {
        public Guid LoggerId { get; set; }

        public DateTime RegisterDate { get; set; }

        public string Method { get; set; }

        public string Origem { get; set; }

        public string Message { get; set; }

        public LogType LogType { get; set; }
    }
}
