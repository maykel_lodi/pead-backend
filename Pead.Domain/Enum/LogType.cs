﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pead.Domain.Enum
{
    public enum LogType
    {
        Info = 1,
        Warning = 2,
        Error = 3,
    }
}
