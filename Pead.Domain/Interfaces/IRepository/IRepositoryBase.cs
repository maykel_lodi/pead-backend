﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Pead.Domain.Interfaces.IRepository
{
    public interface IRepositoryBase<T> where T : class
    {
        void Add(T obj);

        T GetById(int id);

        IEnumerable<T> Find(Expression<Func<T, bool>> expr);

        IEnumerable<T> GetAll();

        void Update(T obj);

        void Remove(T obj);

        void RemoveByExpression(Expression<Func<T, bool>> expr);

        void SaveChanges();

        T Get(Expression<Func<T, bool>> expr);

        bool Exists(Expression<Func<T, bool>> expr);

        T GetAsNoTracking(Expression<Func<T, bool>> expr);

        IEnumerable<T> FindAsNoTracking(Expression<Func<T, bool>> expr);
    }
}