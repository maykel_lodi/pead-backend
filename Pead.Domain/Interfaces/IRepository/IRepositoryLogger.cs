﻿using Pead.Domain.Entities;

namespace Pead.Domain.Interfaces.IRepository
{
    public interface IRepositoryLogger
    {
        void SaveLog(Logger log);
    }
}
