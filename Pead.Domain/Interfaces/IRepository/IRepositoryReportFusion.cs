﻿using Pead.Domain.Entities;

namespace Pead.Domain.Interfaces.IRepository
{
    public interface IRepositoryReportFusion : IRepositoryBase<ReportFusion>
    {
       
    }
}
