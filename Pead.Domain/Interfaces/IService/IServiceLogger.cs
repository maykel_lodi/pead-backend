﻿using Pead.Domain.Enum;
using System;
using Pead.Domain.Entities;

namespace Pead.Domain.Interfaces.IService
{
    public interface IServiceLogger
    {
        void SaveLog(Exception ex, string method, LogType logType, object jsonObject);

        void SaveLog(string message, string method, LogType logType, object jsonObject);

        void SaveLog(Exception ex, string method, LogType logType, string origem = null);

        void SaveLog(string message, string method, LogType logType, string origem = null);

        void SaveLog(string message = "", string methodName = "", string origem = "", LogType logType = LogType.Info);

        void InfoLog(string message);
    }
}
