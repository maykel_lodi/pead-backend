﻿using Pead.API.Models;
using Pead.Domain.Entities;
using Pead.Util;

namespace Pead.Domain.Interfaces.IService
{
    public interface IServiceReportFusion
    {
        Result<bool> Insert(ReportFusionModel report);
        Result<bool> sendReportEmail(ReportPdfFileModel model);
    }
}
