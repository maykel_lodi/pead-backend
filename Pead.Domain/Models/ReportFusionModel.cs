﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pead.API.Models
{
    public class ReportFusionModel
    {
        public Guid id { get; set; }
        public Guid app_id { get; set; }
        public string code { get; set; }
        public string user_id { get; set; }
        public Double user_latitude { get; set; }
        public Double user_longitude { get; set; }
        public DateTime dt_start { get; set; }
        public string description { get; set; }
        public int weld_type_id { get; set; }
        public string climatic_description { get; set; }
        public Double climatic_humidity { get; set; }
        public Double climatic_temperature { get; set; }
        public string material_1_type { get; set; }
        public string material_2_type { get; set; }
        public string material_1_manufacturer { get; set; }
        public string material_2_manufacturer { get; set; }
        public int material_1_external_diameter { get; set; }
        public int material_2_external_diameter { get; set; }
        public string material_1_resin_type { get; set; }
        public string material_2_resin_type { get; set; }
        public Double material_1_pn { get; set; }
        public Double material_2_pn { get; set; }
        public Double material_1_sdr { get; set; }
        public Double material_2_sdr { get; set; }
        public string material_1_lote_number { get; set; }
        public string material_2_lote_number { get; set; }
        public string material_1_lote_number_evidence { get; set; }
        public string material_2_lote_number_evidence { get; set; }
        public string material_1_rule { get; set; }
        public string material_2_rule { get; set; }
        public string equipment_manufacturer_name { get; set; }
        public string equipment_model_name { get; set; }
        public string equipment_serie_number { get; set; }
        public string equipment_serie_number_evidence { get; set; }
        public DateTime equipment_dt_checked { get; set; }
        public string equipment_dt_checked_evidence { get; set; }
        public Double weld_pressure { get; set; }
        public Double weld_pipe_wall { get; set; }
        public int weld_drag_pressure { get; set; }
        public Double weld_initial_cord_height { get; set; }
        public Double weld_heating_plate_temperature { get; set; }
        public string weld_heating_plate_temperature_evidence { get; set; }
        public Double weld_heating_total_time { get; set; }
        public Double weld_cooling_total_time { get; set; }
        public Double weld_plate_out_time { get; set; }
        public Double weld_final_width_strings_side1 { get; set; }
        public Double weld_final_width_strings_side2 { get; set; }
        public DateTime weld_dt_process_start { get; set; }
        public DateTime weld_dt_process_end { get; set; }
        public string equipment_condition { get; set; }
        public Boolean report_weld_approved { get; set; }
        public DateTime dt_aborted { get; set; }
        public string aborted_report_reason { get; set; }
        public DateTime dt_finished { get; set; }
        public DateTime dt_created { get; set; }
        public DateTime dt_modified { get; set; }

    }
}
