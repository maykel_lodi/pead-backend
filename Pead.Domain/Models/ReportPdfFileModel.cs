﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pead.API.Models
{
    public class ReportPdfFileModel
    {
        public IFormFile file { get; set; }
        public string fileName { get; set; }
        public string emailTarget { get; set; }
        public string subject { get; set; }
    }
}
