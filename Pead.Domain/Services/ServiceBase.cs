﻿using Pead.Domain.Interfaces.IRepository;
using Pead.Domain.Interfaces.IService;

namespace Pead.Domain.Services
{
    public class ServiceBase<T> : IServiceBase<T> where T : class
    {
        private readonly IRepositoryBase<T> _repositoryBase;
        private readonly IServiceLogger _serviceLogger;

        public ServiceBase(IRepositoryBase<T> repositoryBase, IServiceLogger serviceLogger)
        {
            _repositoryBase = repositoryBase;
            _serviceLogger = serviceLogger;
        }
    }
}
