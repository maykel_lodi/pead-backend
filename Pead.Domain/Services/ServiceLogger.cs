﻿using Newtonsoft.Json;
using Pead.Domain.Entities;
using Pead.Domain.Enum;
using Pead.Domain.Interfaces;
using Pead.Domain.Interfaces.IRepository;
using Pead.Domain.Interfaces.IService;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pead.Domain.Services
{
    public class ServiceLogger : IServiceLogger
    {
        private readonly IRepositoryLogger _repositoryLogger;

        public ServiceLogger(IRepositoryLogger repositoryLogger)
        {
            _repositoryLogger = repositoryLogger;
        }

        public void InfoLog(string message)
        {
            var log = new Logger
            {
                RegisterDate = DateTime.Now,
                LogType = LogType.Info,
                LoggerId = Guid.NewGuid(),
                Message = message
            };

            _repositoryLogger.SaveLog(log);
        }

        public void SaveLog(string message = "", string methodName = "", string origem = "", LogType tipoLog = LogType.Info)
        {
            var log = new Logger()
            {
                LoggerId = Guid.NewGuid(),
                RegisterDate = DateTime.Now,
                Method = methodName,
                Message = message,
                Origem = origem,
                LogType = tipoLog
            };

            _repositoryLogger.SaveLog(log);
        }

        public void SaveLog(string message, string method, LogType logType, string origem = null)
        {
            var log = new Logger()
            {
                LoggerId = Guid.NewGuid(),
                RegisterDate = DateTime.Now,
                Method = method,
                Message = message,
                LogType = logType,
                Origem = origem
            };

            _repositoryLogger.SaveLog(log);
        }

        public void SaveLog(Exception ex, string method, LogType tipoLog, string origem = null)
        {
            var log = new Logger()
            {
                RegisterDate = DateTime.Now,
                Method = method,
                Message = PrepareExceptionString(ex),
                LogType = tipoLog,
                Origem = origem
            };

            _repositoryLogger.SaveLog(log);
        }

        public void SaveLog(Exception ex, string method, LogType logType, object jsonObject)
        {
            var log = new Logger()
            {
                LoggerId = Guid.NewGuid(),
                RegisterDate = DateTime.Now,
                Message = PrepareExceptionString(ex),
                Method = method,
                Origem = JsonConvert.SerializeObject(jsonObject),
                LogType = logType
            };

            _repositoryLogger.SaveLog(log);
        }

        public void SaveLog(string message, string method, LogType logType, object jsonObject)
        {
            var log = new Logger()
            {
                LoggerId = Guid.NewGuid(),
                Message = message,
                RegisterDate = DateTime.Now,
                Method = method,
                Origem = JsonConvert.SerializeObject(jsonObject),
                LogType = logType
            };

            _repositoryLogger.SaveLog(log);
        }

        private string PrepareExceptionString(Exception ex)
        {

            string exceptionMessage = $"{ex.Message}\r\n\t{ex}\r\n---------------------------\r\n";

            if (ex.InnerException != null)
                exceptionMessage += $"\t{this.PrepareExceptionString(ex.InnerException)}";

            return exceptionMessage;

        }
    }
}
