﻿using AutoMapper;
using Pead.API.Models;
using Pead.Domain.Entities;
using Pead.Domain.Enum;
using Pead.Domain.Interfaces.IRepository;
using Pead.Domain.Interfaces.IService;
using Pead.Util;
using System;
using System.IO;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading.Tasks;

namespace Pead.Domain.Services
{
    public class ServiceReportFusion : IServiceReportFusion
    {
        private readonly IRepositoryReportFusion _repositoryReportFusion;
        private readonly IServiceLogger _serviceLogger;
        private readonly IMapper _mapper;

        public ServiceReportFusion( IMapper mapper, 
                                    IServiceLogger serviceLogger,
                                    IRepositoryReportFusion repositoryReportFusion)
        {
            _repositoryReportFusion = repositoryReportFusion;
            _serviceLogger = serviceLogger;
            _mapper = mapper;
        }

        public Result<bool> sendReportEmail(ReportPdfFileModel model)
        {
            var result = new Result<bool>();

            var validateResult = ValidateReportPdfModel(model);
            if (!validateResult.IsSuccess)
            {
                result.IsSuccess = false;
                result.Mensagens.AddRange(validateResult.Mensagens);
                return result;
            }

            try
            {
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress("pead@gmail.com");
                mail.To.Add(model.emailTarget);
                mail.Subject = model.subject;
                mail.Body = $"Relatório de solda exportado pelo aplicativo 'Pead - Auditoria Digital' em {DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")}";

                var memoryStream = new MemoryStream();
                model.file.CopyTo(memoryStream);
                memoryStream.Position = 0;

                var contentType = new ContentType(MediaTypeNames.Application.Pdf);

                var att = new Attachment(memoryStream, contentType);
                att.ContentDisposition.FileName = model.fileName + ".pdf";
                mail.Attachments.Add(att);

                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
                SmtpServer.Port = 587;
                SmtpServer.UseDefaultCredentials = false;
                SmtpServer.Credentials = new System.Net.NetworkCredential("pead.ad.app@gmail.com", "pead.ad.app2019");
                SmtpServer.EnableSsl = true;
                SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                SmtpServer.Send(mail);
            }catch(Exception ex)
            {
                result.Data = false;
                result.IsSuccess = false;
                result.Mensagens.Add("Houve uma falha no envio do relatório. " + ex.Message);
                return result;
            }

            result.Data = true;
            result.Mensagens.Add("Relatório enviado com sucesso!");
            return result;
        }

        public Result<bool> Insert(ReportFusionModel report)
        {
            var result = new Result<bool>();

            try
            {
                var map = _mapper.Map<ReportFusion>(report);

                _repositoryReportFusion.Add(map);
                result.Data = true;
            }
            catch (Exception ex)
            {
                _serviceLogger.SaveLog(ex, "ServiceReportFusion:Insert", LogType.Error, report);
            }

            return result;
        }

        private Result<bool> ValidateReportPdfModel(ReportPdfFileModel model)
        {
            var result = new Result<bool>();

            if (model.file == null)
            {
                result.IsSuccess = false;
                result.Mensagens.Add("É obrigatório o envio do pdf");
            }
            if (string.IsNullOrEmpty(model.emailTarget))
            {
                result.IsSuccess = false;
                result.Mensagens.Add("É obrigatório o envio do email destino");
            }
            if (string.IsNullOrEmpty(model.fileName))
            {
                result.IsSuccess = false;
                result.Mensagens.Add("É obrigatório o envio do nome do pdf");
            }
            if (string.IsNullOrEmpty(model.subject))
            {
                result.IsSuccess = false;
                result.Mensagens.Add("É obrigatório o envio do assunto do email");
            }

            return result;
        }
    }
}
