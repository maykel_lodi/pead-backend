﻿using Microsoft.EntityFrameworkCore;
using Pead.Domain.Entities;
using Pead.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pead.Infra.Context
{
    public class PeadDbContext: DbContext, IDbContext { 
        public PeadDbContext(DbContextOptions<PeadDbContext> options) : base(options) { }
        
        public DbSet<Logger> Logger { get; set; }
        public DbSet<ReportFusion> ReportFusion { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Logger>().HasKey(p => p.LoggerId);
            modelBuilder.Entity<ReportFusion>().HasKey(r => r.id);

            base.OnModelCreating(modelBuilder);
        }
    }
}
