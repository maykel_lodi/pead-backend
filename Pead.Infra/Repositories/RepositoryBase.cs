﻿using Microsoft.EntityFrameworkCore;
using Pead.Domain;
using Pead.Domain.Interfaces;
using Pead.Domain.Interfaces.IRepository;
using Pead.Infra.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Pead.Infra.Repositories
{
    public class RepositoryBase<T> : IRepositoryBase<T> where T : class
    {
        protected PeadDbContext DbConnect;

       public RepositoryBase(IDbContext dbContext)
        {
            DbConnect = (PeadDbContext)dbContext;
        }

        public virtual void Add(T obj)
        {
            DbConnect.Set<T>().Add(obj);
        }

        public virtual IEnumerable<T> GetAll()
        {
            return DbConnect.Set<T>().ToList();
        }

        public virtual void Update(T obj)
        {
            DbConnect.Entry(obj).State = EntityState.Modified;
        }

        public virtual void Remove(T obj)
        {
            DbConnect.Set<T>().Remove(obj);
        }

        public void RemoveByExpression(Expression<Func<T, bool>> expr)
        {
            var itemsToRemove = DbConnect.Set<T>().Where(expr);
            if (itemsToRemove != null && itemsToRemove.Any())
            {
                DbConnect.Set<T>().RemoveRange(itemsToRemove);
            }
        }

        public IEnumerable<T> Find(Expression<Func<T, bool>> expr)
        {
            return DbConnect.Set<T>().Where(expr);
        }

        public T Get(Expression<Func<T, bool>> expr)
        {
            return DbConnect.Set<T>().FirstOrDefault(expr);
        }

        public virtual T GetById(int id)
        {
            return DbConnect.Set<T>().Find(id);
        }

        public bool Exists(Expression<Func<T, bool>> expr)
        {
            return DbConnect.Set<T>().Any(expr);
        }

        public T GetAsNoTracking(Expression<Func<T, bool>> expr)
        {
            return DbConnect.Set<T>().Where(expr).AsNoTracking().FirstOrDefault();
        }

        public IEnumerable<T> FindAsNoTracking(Expression<Func<T, bool>> expr)
        {
            return DbConnect.Set<T>().Where(expr).AsNoTracking();
        }

        public void SaveChanges()
        {
            DbConnect.SaveChanges();
        }
    }
}