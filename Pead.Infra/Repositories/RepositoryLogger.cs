﻿using Pead.Domain.Entities;
using Pead.Domain.Interfaces;
using Pead.Domain.Interfaces.IRepository;

namespace Pead.Infra.Repositories
{
    public class RepositoryLogger : RepositoryBase<Logger>, IRepositoryLogger
    {
        public RepositoryLogger(IDbContext dbContext) : base(dbContext)
        {

        }
        public void SaveLog(Logger log)
        {
            DbConnect.Logger.Add(log);
            DbConnect.SaveChanges();
        }
    }
}