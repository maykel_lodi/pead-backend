﻿using Pead.Domain.Entities;
using Pead.Domain.Interfaces;
using Pead.Domain.Interfaces.IRepository;
using Pead.Infra.Context;

namespace Pead.Infra.Repositories
{
    public class RepositoryReportFusion : RepositoryBase<ReportFusion>, IRepositoryReportFusion
    {

       protected PeadDbContext DbConnect;

        public RepositoryReportFusion(IDbContext dbContext) : base(dbContext)
        {
            DbConnect = (PeadDbContext)dbContext;
        }

        

    }
}
