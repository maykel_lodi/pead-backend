﻿using Microsoft.Extensions.DependencyInjection;
using Pead.Domain.Interfaces;
using Pead.Domain.Services;
using Pead.Infra.Context;
using Pead.Infra.Repositories;
using System.Linq;

namespace Pead.Infra
{
    public class Services
    {

        public static void Init(IServiceCollection services)
        {
            RepositoryCore(services);
            DomainCore(services);
            DbContext(services);
        }

        static void DomainCore(IServiceCollection services)
        {
            var repositoryAssembly = typeof(ServiceBase<>).Assembly;
            var registrationsRepository = from type in repositoryAssembly.GetExportedTypes()
                                          where
                                              (type.Namespace == "Pead.Domain.Services") && !type.IsGenericType
                                          where type.GetInterfaces().Any(x => !x.IsGenericType)
                                          select new { Service = type.GetInterfaces().Single(x => !x.IsGenericType), Implementation = type };

            foreach (var reg in registrationsRepository)
            {
                services.AddTransient(reg.Service, reg.Implementation);
            }
        }

        static void RepositoryCore(IServiceCollection services)
        {
            var repositoryAssembly = typeof(RepositoryBase<>).Assembly;
            var registrationsRepository = from type in repositoryAssembly.GetExportedTypes()
                                          where
                                              (type.Namespace == "Pead.Infra.Repositories") && !type.IsGenericType
                                          where type.GetInterfaces().Any(x => !x.IsGenericType)
                                          select new { Service = type.GetInterfaces().Single(x => !x.IsGenericType), Implementation = type };

            foreach (var reg in registrationsRepository)
            {
                services.AddTransient(reg.Service, reg.Implementation);
            }
        }
   
        static void DbContext(IServiceCollection services)
        {
            services.AddTransient<IDbContext, PeadDbContext>();
        }


    }
}
