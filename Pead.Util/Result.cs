﻿using System;
using System.Collections.Generic;

namespace Pead.Util
{
    public class Result<T>
    {

        public Result()
        {
            if (typeof(T) == typeof(string))
                Data = default(T);
            else
                Data = System.Activator.CreateInstance<T>();

            IsSuccess = true;
        }

        public T Data { get; set; }

        private List<string> _mensagens;
        public List<string> Mensagens
        {
            get { return _mensagens ?? (_mensagens = new List<string>()); }
            set { _mensagens = value; }
        }
        public bool IsSuccess { get; set; }

    }
}
