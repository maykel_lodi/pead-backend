﻿using Microsoft.AspNetCore.Mvc;
using Pead.API.Models;
using Pead.Domain.Interfaces.IService;
using Pead.Util;
using System;

namespace Pead.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReportController : ControllerBase
    {

        private readonly IServiceReportFusion _serviceReportFusion;

        public ReportController(IServiceReportFusion serviceReportFusion)
        {
            _serviceReportFusion = serviceReportFusion;
        }



        // GET api/valuesfsdf
        [HttpGet]
        public Result<string> Get()
        {
            var result = new Result<string>();
            result.Data = "Teste sucesso!";
            return result;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public Result<bool> Post(ReportFusionModel model)
        {
            var result = new Result<bool>();
            try
            {
                result = _serviceReportFusion.Insert(model);
            }
            catch(Exception ex)
            {
                result.IsSuccess = false;
                result.Mensagens.Add("Erro ao salvar relatório de Eletro Fusão: " + ex.Message);
            }
            return result;
        }

        // POST api/values
        [HttpPost]
        [Route("sendReportEmail")]
        public Result<bool> sendReportEmail([FromForm] ReportPdfFileModel model)
        {
            var result = new Result<bool>();
            try
            {
                result = _serviceReportFusion.sendReportEmail(model);
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.Mensagens.Add("Erro ao enviar email com o relatório: " + ex.Message);
            }
            return result;
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
