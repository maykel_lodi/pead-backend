﻿using System;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Pead.Domain.Interfaces.IRepository;
using Pead.Domain.Interfaces.IService;
using Pead.Domain.Services;
using Pead.Infra.Context;
using Pead.Infra.Repositories;

namespace Pead
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            Pead.Infra.Services.Init(services);

            //services.AddSingleton<IServiceReportFusion, ServiceReportFusion>();
            //services.AddSingleton<IRepositoryReportFusion, RepositoryReportFusion>();

            services
                   .AddMvc()
                   .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                   .AddControllersAsServices()
                   .AddJsonOptions(c => c.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore);

            services.AddDbContext<PeadDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("Pead")));

            services
                .AddMvcCore()
                .AddApiExplorer();

            services.AddAutoMapper(typeof(Startup));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
